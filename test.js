const {
    success,
    warning,
    info,
    error,
    log
} = require('./app');

success('Hola success!');
warning('Hola warning!');
info('Hola info!');
error('Hola error!');
log('Hola log!');